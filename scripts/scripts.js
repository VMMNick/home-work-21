function drawCircles() {
  var diameter = prompt("Введіть діаметр кола:");
  var circleContainer = document.getElementById("circleContainer");
  circleContainer.innerHTML = ""; // Очистити попередні кола перед створенням нових

  for (var i = 0; i < 100; i++) {
    var circle = document.createElement("div");
    circle.className = "circle";
    circle.style.width = diameter + "px";
    circle.style.height = diameter + "px";
    circle.style.backgroundColor = getRandomColor();

    // Додаємо обробник події на кожне коло
    circle.onclick = function () {
      this.style.display = "none"; // Зникаємо при кліку
      rearrangeCircles(); // Пересортуємо інші кола
    };

    circleContainer.appendChild(circle);
  }
}

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function rearrangeCircles() {
  var circles = document.getElementsByClassName("circle");
  for (var i = 0; i < circles.length; i++) {
    if (circles[i].style.display === "none") {
      for (var j = i + 1; j < circles.length; j++) {
        if (circles[j].style.display !== "none") {
          circles[i].style.backgroundColor = circles[j].style.backgroundColor;
          circles[j].style.display = "none";
          break;
        }
      }
    }
  }
}
